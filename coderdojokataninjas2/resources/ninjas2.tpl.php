<div class="kata-ninjas">

	<div class="row kata-ninjas-header">
		<div class="col-xs-12">
			<h2>Kata for Ninjas</h2>
		</div>
	</div>

	<div style="margin-top: 2rem">
		<ul class="nav nav-tabs">
			<li class="active"><a data-toggle="tab" href="#sectionA">Games</a></li>
			<li><a data-toggle="tab" href="#sectionB">Tutorials</a></li>
			<li><a data-toggle="tab" href="#sectionB">Videos</a></li>
		</ul>
		<div class="tab-content">
			<div id="sectionA" class="tab-pane fade in active">
				<div class="panel-body">
					<div class="row kata-ninjas-group">

						<!-- languages -->
						<div class="col-xs-12 kata-ninjas-heading">
							<h4>Top 5 games</h4>
						</div>
						<div class="col-xs-12 kata-ninjas-content">
							<div class="kata-tags single-column">

								<div class="ratings-panel">
									<div class="description">Lorem Ipsum is simply dummy text of
										the printing and typesetting industry. Lorem Ipsum has been
										the industry's standard dummy text ever since the 1500s, when
										an unknown printer took a galley of type and scrambled it to
										make a type specimen book. It has survived not only five
										centuries, but also the leap into electronic typesetting,
										remaining essentially unchanged. It was popularised in the
										1960s with the release of Letraset sheets containing Lorem
										Ipsum passages, and more recently with desktop publishing
										software like Aldus PageMaker including versions of Lorem
										Ipsum.</div>
									<div class="preview">
										<img src="src/images/SpongeBob.jpg" /> <span
											class="star-rating"> <input type="radio" name="rating"
											value="1" /><i></i> <input type="radio" name="rating"
											value="2" /><i></i> <input type="radio" name="rating"
											value="3" /><i></i> <input type="radio" name="rating"
											value="4" /><i></i> <input type="radio" name="rating"
											value="5" /><i></i>
										</span>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="panel-body">
					<div class="row kata-ninjas-group">
						<!-- alpha-filter -->
						<div class="col-xs-12 kata-ninjas-heading">
							<h4>Games by Alphabet</h4>
						</div>
						<div class="col-xs-12 kata-ninjas-content">
							<div class="alpha-filter">
								<div class="filters">
									<span class="letter" data-filter="A"><a href="#">A</a></span> <span
										class="letter" data-filter="B"><a href="#">B</a></span> <span
										class="letter" data-filter="C"><a href="#">C</a></span> <span
										class="letter" data-filter="D"><a href="#">D</a></span> <span
										class="letter" data-filter="E"><a href="#">E</a></span> <span
										class="letter" data-filter="F"><a href="#">F</a></span> <span
										class="letter" data-filter="G"><a href="#">G</a></span> <span
										class="letter" data-filter="H"><a href="#">H</a></span> <span
										class="letter" data-filter="I"><a href="#">I</a></span> <span
										class="letter" data-filter="J"><a href="#">J</a></span> <span
										class="letter" data-filter="K"><a href="#">K</a></span> <span
										class="letter" data-filter="L"><a href="#">L</a></span> <span
										class="letter" data-filter="M"><a href="#">M</a></span> <span
										class="letter" data-filter="N"><a href="#">N</a></span> <span
										class="letter" data-filter="O"><a href="#">O</a></span> <span
										class="letter" data-filter="P"><a href="#">P</a></span> <span
										class="letter" data-filter="Q"><a href="#">Q</a></span> <span
										class="letter" data-filter="R"><a href="#">R</a></span> <span
										class="letter" data-filter="S"><a href="#">S</a></span> <span
										class="letter" data-filter="T"><a href="#">T</a></span> <span
										class="letter" data-filter="U"><a href="#">U</a></span> <span
										class="letter" data-filter="V"><a href="#">V</a></span> <span
										class="letter" data-filter="W"><a href="#">W</a></span> <span
										class="letter" data-filter="X"><a href="#">X</a></span> <span
										class="letter" data-filter="Y"><a href="#">Y</a></span> <span
										class="letter" data-filter="Z"><a href="#">Z</a></span>
								</div>
								<div class="kata-tags">
									<div class="kata-tag" data-filter="A">
										<a href="#">3D &amp; Animation <span>3210</span></a>
									</div>
									<div class="kata-tag" data-filter="G">
										<a href="#">Games <span>3210</span></a>
									</div>
									<div class="kata-tag" data-filter="S">
										<a href="#">Social <span>3210</span></a>
									</div>
									<div class="kata-tag" data-filter="A">
										<a href="#">Audio &amp; Music <span>42</span></a>
									</div>
									<div class="kata-tag" data-filter="M">
										<a href="#">Mobile <span>42</span></a>
									</div>
									<div class="kata-tag" data-filter="S">
										<a href="#">Security <span>42</span></a>
									</div>
									<div class="kata-tag" data-filter="D">
										<a href="#">Design <span>3</span></a>
									</div>
									<div class="kata-tag" data-filter="P">
										<a href="#">Programming Foundations <span>3</span></a>
									</div>
									<div class="kata-tag" data-filter="O">
										<a href="#">Operating Systems <span>3</span></a>
									</div>
									<div class="kata-tag" data-filter="D">
										<a href="#">Databases <span>15</span></a>
									</div>
									<div class="kata-tag" data-filter="P">
										<a href="#">Programming Languages <span>15</span></a>
									</div>
									<div class="kata-tag" data-filter="H">
										<a href="#">Hardware Kits <span>15</span></a>
									</div>
									<div class="kata-tag" data-filter="D">
										<a href="#">Desktop Apps <span>15</span></a>
									</div>
									<div class="kata-tag" data-filter="B">
										<a href="#">Big Data <span>15</span></a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div id="sectionB" class="tab-pane fade">
				<div class="panel-body">
					<div class="row kata-ninjas-group">

						<!-- languages -->
						<div class="col-xs-12 kata-ninjas-heading">
							<h4>Top 5 games</h4>
						</div>
						<div class="col-xs-12 kata-ninjas-content">
							<img src="src/images/ninja-cat.jpg" />
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>