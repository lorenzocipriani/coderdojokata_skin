<div class="kata-mentors">

	<div class="row kata-category-title">
		<div class="col-xs-12">
			<h2>Kata for Mentors</h2>
		</div>
	</div>
	<div class="row kata-box">
		<!-- languages -->
		<div class="col-xs-12 kata-box-title">
			<h4>Tutorials in different languages</h4>
		</div>
		<div class="col-xs-12 kata-box-content">
			<div class="kata-tags">
				<!-- div class="kata-tags single-column" -->
				<div class="kata-tag">
					<a href="#">English <span>3210</span></a>
				</div>
				<div class="kata-tag">
					<a href="#">Italian <span>42</span></a>
				</div>
			</div>
		</div>
	</div>
	<div class="row kata-box">
		<!-- categories -->
		<div class="col-xs-12 kata-box-title">
			<h4>Categories</h4>
		</div>
		<div class="col-xs-12 kata-box-content">
			<div class="kata-tags">
				<div class="kata-tag">
					<a href="#">3D &amp; Animation <span>3210</span></a>
				</div>
				<div class="kata-tag">
					<a href="#">Games <span>3210</span></a>
				</div>
				<div class="kata-tag">
					<a href="#">Social <span>3210</span></a>
				</div>
				<div class="kata-tag">
					<a href="#">Audio &amp; Music <span>42</span></a>
				</div>
				<div class="kata-tag">
					<a href="#">Mobile <span>42</span></a>
				</div>
				<div class="kata-tag">
					<a href="#">Security <span>42</span></a>
				</div>
				<div class="kata-tag">
					<a href="#">Design <span>3</span></a>
				</div>
				<div class="kata-tag">
					<a href="#">Programming Foundations <span>3</span></a>
				</div>
				<div class="kata-tag">
					<a href="#">Operating Systems <span>3</span></a>
				</div>
				<div class="kata-tag">
					<a href="#">Databases <span>15</span></a>
				</div>
				<div class="kata-tag">
					<a href="#">Programming Languages <span>15</span></a>
				</div>
				<div class="kata-tag">
					<a href="#">Hardware Kits <span>15</span></a>
				</div>
				<div class="kata-tag">
					<a href="#">Desktop Apps <span>15</span></a>
				</div>
				<div class="kata-tag">
					<a href="#">Big Data <span>15</span></a>
				</div>
			</div>
		</div>
	</div>
	<div class="row kata-box">
		<!-- alpha-filter -->
		<div class="col-xs-12 kata-box-title">
			<h4>Software by Alphabet</h4>
		</div>
		<div class="col-xs-12 kata-box-content">
			<div class="alpha-filter">
				<div class="filters clearfix">
					<span class="letter" data-filter="A"><a href="#">A</a></span> <span
						class="letter" data-filter="B"><a href="#">B</a></span> <span
						class="letter" data-filter="C"><a href="#">C</a></span> <span
						class="letter" data-filter="D"><a href="#">D</a></span> <span
						class="letter" data-filter="E"><a href="#">E</a></span> <span
						class="letter" data-filter="F"><a href="#">F</a></span> <span
						class="letter" data-filter="G"><a href="#">G</a></span> <span
						class="letter" data-filter="H"><a href="#">H</a></span> <span
						class="letter" data-filter="I"><a href="#">I</a></span> <span
						class="letter" data-filter="J"><a href="#">J</a></span> <span
						class="letter" data-filter="K"><a href="#">K</a></span> <span
						class="letter" data-filter="L"><a href="#">L</a></span> <span
						class="letter" data-filter="M"><a href="#">M</a></span> <span
						class="letter" data-filter="N"><a href="#">N</a></span> <span
						class="letter" data-filter="O"><a href="#">O</a></span> <span
						class="letter" data-filter="P"><a href="#">P</a></span> <span
						class="letter" data-filter="Q"><a href="#">Q</a></span> <span
						class="letter" data-filter="R"><a href="#">R</a></span> <span
						class="letter" data-filter="S"><a href="#">S</a></span> <span
						class="letter" data-filter="T"><a href="#">T</a></span> <span
						class="letter" data-filter="U"><a href="#">U</a></span> <span
						class="letter" data-filter="V"><a href="#">V</a></span> <span
						class="letter" data-filter="W"><a href="#">W</a></span> <span
						class="letter" data-filter="X"><a href="#">X</a></span> <span
						class="letter" data-filter="Y"><a href="#">Y</a></span> <span
						class="letter" data-filter="Z"><a href="#">Z</a></span>
				</div>
				<div class="kata-tags filter-elements">
					<div class="kata-tag" data-filter="A">
						<a href="#">3D &amp; Animation <span>3210</span></a>
					</div>
					<div class="kata-tag" data-filter="G">
						<a href="#">Games <span>3210</span></a>
					</div>
					<div class="kata-tag" data-filter="S">
						<a href="#">Social <span>3210</span></a>
					</div>
					<div class="kata-tag" data-filter="A">
						<a href="#">Audio &amp; Music <span>42</span></a>
					</div>
					<div class="kata-tag" data-filter="M">
						<a href="#">Mobile <span>42</span></a>
					</div>
					<div class="kata-tag" data-filter="S">
						<a href="#">Security <span>42</span></a>
					</div>
					<div class="kata-tag" data-filter="D">
						<a href="#">Design <span>3</span></a>
					</div>
					<div class="kata-tag" data-filter="P">
						<a href="#">Programming Foundations <span>3</span></a>
					</div>
					<div class="kata-tag" data-filter="O">
						<a href="#">Operating Systems <span>3</span></a>
					</div>
					<div class="kata-tag" data-filter="D">
						<a href="#">Databases <span>15</span></a>
					</div>
					<div class="kata-tag" data-filter="P">
						<a href="#">Programming Languages <span>15</span></a>
					</div>
					<div class="kata-tag" data-filter="H">
						<a href="#">Hardware Kits <span>15</span></a>
					</div>
					<div class="kata-tag" data-filter="D">
						<a href="#">Desktop Apps <span>15</span></a>
					</div>
					<div class="kata-tag" data-filter="B">
						<a href="#">Big Data <span>15</span></a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>